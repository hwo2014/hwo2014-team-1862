using System;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using Newtonsoft.Json;
using BotTools;
using BotTools.Messages;
using BotTools.Messages.HelperClasses;

public class Bot {

	public static string LatestReceavedLine = null;
	public static string LatestSentLine = null;

	public static void Main(string[] args) {
	    string host = Tools.GetArg(args, 0, Settings.DefaultHost);
		int port = Tools.GetIntArg(args, 1, Settings.DefaultPort);
		string botName = Tools.GetArg(args, 2, Settings.DefaultBotName);
		string botKey = Tools.GetArg(args, 3, Settings.DefaultBotKey);
		
		if (args != null && args.Where(x => x.ToLower() == "debug").Any())
		{
			Settings.Debug = true;
		}
		if (args != null && args.Where(x => x.ToLower() == "nolog").Any())
		{
			Settings.NoLog = true;
		}
		if (args != null && args.Where(x => x.ToLower() == "showtics").Any())
		{
			Settings.ShowTics = true;
		}
		

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			
			//client.Client.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.KeepAlive, true);
			
			if (Settings.ReceiveTimeout.HasValue) client.ReceiveTimeout = Settings.ReceiveTimeout.Value;
			if (Settings.SendTimeout.HasValue) client.SendTimeout = Settings.SendTimeout.Value;

			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			//if (Settings.Test) new Bot(reader, writer, new JoinRace(botName, botKey, Settings.DefaultTrackName));
			//else 
				new Bot(reader, writer, new Join(botName, botKey));
		}
	}

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, IMessage firstMessageToSend) {
		this.writer = writer;
		string line;

		send(firstMessageToSend);

		BotEnvironment env = new BotEnvironment();

		Int64 tick = 0;

		int carPositionCount = 0;

		while((line = reader.ReadLine()) != null) {

			if (Settings.Debug) Log.WriteToLog(line, LogEntryType.ReceivedMessage);

			if (Settings.ShowTics) Console.WriteLine(tick.ToString());
			tick++;

			LatestReceavedLine = line;

			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			//var msg = Message.FromJson<Message>(line);

			//var angle = Tools.GetPropertyValue(msg.data, "angle");
			//if (angle != null) Console.WriteLine("Angle : " + angle);

			
			if (msg.msgTypeEnum == MessageType.carPositions && !msg.gameTick.HasValue)
			{
				msg.msgTypeEnum = MessageType.skip;
			}

			switch(msg.msgTypeEnum)
			{

				#region carPositions
				// 5
				case MessageType.carPositions:

					CarPosition position = Tools.FromJson<CarPosition>(line);

					if (position != null)
					{
						env.CarPosition = position;
						var fullCar = position.data.Where(c => c.GetId() == env.MyCar.GetId()).FirstOrDefault();
						if (fullCar != null) env.MyCar = fullCar;
					}

					if (env.CarPosition != null && env.Track != null && env.MyCar.piecePosition != null && env.MyCar.piecePosition.pieceIndex.HasValue && env.MyCar.piecePosition.inPieceDistance.HasValue)
					{
						var changeLane = BasicLogic.ChangeLane(env.Track, env.MyCar.piecePosition.pieceIndex.Value, env.MyCar.piecePosition.inPieceDistance.Value, env.MyCar.piecePosition.lane.startLaneIndex.Value);
						if (changeLane.HasValue)
						{
							send(new SwitchLane(changeLane.Value));
							break;
						}

						double? DistanceToNextStraight = null;
						var distanceToNextCurve = BasicLogic.DistanceToNextCurve(env.Track, env.MyCar.piecePosition.pieceIndex.Value, env.MyCar.piecePosition.inPieceDistance.Value);
						double? cl = null;
						double ang = 0;

						//Console.WriteLine("distanceToNextCurve = " + Tools.DoubleToString(distanceToNextCurve.Value) + "          " + env.CurrentThrottle + "                 " );

						Piece piece = env.Track.pieces != null && env.MyCar.piecePosition.pieceIndex.HasValue && env.Track.pieces.Length - 1 >= env.MyCar.piecePosition.pieceIndex.Value ? env.Track.pieces[env.MyCar.piecePosition.pieceIndex.Value] : null;

						//if (distanceToNextCurve.HasValue && distanceToNextCurve.Value < 200 && (piece == null || !piece.isCurve))
						if (distanceToNextCurve.HasValue && distanceToNextCurve.Value < 200 && (piece == null || piece.isCurve))
						{
							bool notSet = true;
							int curvePiecesCount;
							double angle;

							double? laneCorrection = null;
							try
							{
								laneCorrection = env.Track.lanes[env.MyCar.piecePosition.lane.startLaneIndex.Value].distanceFromCenter;
							}
							catch { laneCorrection = 0; }

							double? curveLength = BasicLogic.LengthOfNextCurve(env.Track, env.MyCar.piecePosition.pieceIndex.Value, env.MyCar.piecePosition.inPieceDistance.Value, laneCorrection.Value, out curvePiecesCount, out angle);
							cl = curveLength;
							ang = angle;
							//Console.WriteLine(curveLength.Value);

							//Console.WriteLine("Angle = " + angle);
							//Console.WriteLine("" + laneCorrection.Value);

							//if (curveLength.HasValue && curveLength.Value > 0)
							{

								//Console.WriteLine("" + Tools.DoubleToString(curveLength) + "        " + Tools.DoubleToString(angle));

								//if ((angle < 0 && angle > -45) || (angle > 0 && angle < 45) || curvePiecesCount <= 2)
								if ((angle < 0 && angle > -45) || (angle > 0 && angle < 45))
								{
									//Console.WriteLine(String.Format(
									//	"{0} # {1} # {2} # {3} # {4}",
									//	env.MyCar.piecePosition.pieceIndex.Value,
									//	angle,
									//	curvePiecesCount,
									//	curveLength.HasValue ? curveLength.Value.ToString() : "null",
									//	env.CurrentThrottle
									//));
									//if (changeLane.HasValue) env.CurrentThrottle = 0.8;
									//else env.CurrentThrottle = 0.84;

									if (changeLane.HasValue) env.CurrentThrottle = 0.7;
									else env.CurrentThrottle = 0.74;

									if (curveLength.HasValue && curveLength.Value > 350)
									{
										var a = angle >= 0 ? angle : angle * -1;
										env.CurrentThrottle = env.CurrentThrottle - (a / curveLength.Value);
									}
									else if (curveLength.HasValue && curveLength < 100)
									{
										var a = angle >= 0 ? angle : angle * -1;
										env.CurrentThrottle = env.CurrentThrottle - (a / curveLength.Value);
									}
									else
									{
										var a = angle >= 0 ? angle : angle * -1;
										env.CurrentThrottle = env.CurrentThrottle + (a / curveLength.Value);
									}

									notSet = false;
								}
								else if (angle >= 45 || angle <= -45)
								{
									if (angle > 60 || angle < -60) env.CurrentThrottle = 0.4;
									else env.CurrentThrottle = 0.5;

									if (curveLength.HasValue && curveLength.Value > 200)
									{
										var a = angle >= 0 ? angle : angle * -1;
										env.CurrentThrottle = env.CurrentThrottle - (a / curveLength.Value);
									}
									else if (curveLength.HasValue && curveLength < 100)
									{
										var a = angle >= 0 ? angle : angle * -1;
										env.CurrentThrottle = env.CurrentThrottle - (a / curveLength.Value);
									}

									notSet = false;
								}
							}


							if (notSet)
							{
								//if (changeLane.HasValue) env.CurrentThrottle = 0.9;
								//else env.CurrentThrottle = 0.95;
								if (changeLane.HasValue) env.CurrentThrottle = 0.8;
								else env.CurrentThrottle = 0.85;
							}
						}
						else if (piece != null && piece.isCurve)
						{
							DistanceToNextStraight = BasicLogic.DistanceToNextStraight(env.Track, env.MyCar.piecePosition.pieceIndex.Value, env.MyCar.piecePosition.inPieceDistance.Value);

							if (piece.angle.HasValue)
							{
								if ((piece.angle.Value < 0 && piece.angle.Value > -45) || (piece.angle.Value > 0 && piece.angle.Value < 45))
								{
									env.CurrentThrottle = 0.7;
								}
								else
								{
									env.CurrentThrottle = 0.6;
								}
							}

							if (DistanceToNextStraight.HasValue)
							{
								double k = 100;
								if (env.CurrentThrottle > 0.6)
								{
									k = 100 - ((env.CurrentThrottle - 0.6) * 100);
								}

								if (DistanceToNextStraight.Value < k) env.CurrentThrottle = env.CurrentThrottle + 0.1;

								//if (env.CurrentThrottle > 1) env.CurrentThrottle = 1;
							}
							else env.CurrentThrottle = 1;
						}
						//else if (distanceToNextCurve.HasValue && distanceToNextCurve.Value <= 210)
						//	env.CurrentThrottle = 0.7;
						else
						{
							env.CurrentThrottle = 1;
						}
							

						if (Settings.Debug)
						{
							Log.WriteToLog("distanceToNextCurve=" + distanceToNextCurve.Value, LogEntryType.General);
							Console.WriteLine(String.Format(
								"{0} --- {1}",
								distanceToNextCurve.Value,
								env.CurrentThrottle
							));
						}

						if (env.CurrentThrottle > 1) env.CurrentThrottle = 1;
						else if (env.CurrentThrottle <= 0) env.CurrentThrottle = 0.3;

						//if (distanceToNextCurve.HasValue && distanceToNextCurve.Value > 600 && env.TurboAvailable && (!cl.HasValue || ang == 0))
						if (distanceToNextCurve.HasValue && env.TurboAvailable && ((distanceToNextCurve.Value >= 300 && !piece.isCurve) ||
							(distanceToNextCurve.Value >= 400 && DistanceToNextStraight.HasValue && DistanceToNextStraight.Value <= 80))
						)
						{
							send(new Turbo());
							env.TurboAvailable = false;
							break;
						}

						if (Settings.Test)
						{
							Console.WriteLine(String.Format(
								"{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}",
								tick, // 0
								Tools.DoubleToString(cl), // 1 curve length
								Tools.DoubleToString(ang), // 2 angle
								Tools.DoubleToString(piece.angle), // 3
								Tools.DoubleToString(distanceToNextCurve), //4
								Tools.DoubleToString(DistanceToNextStraight), // 5
								Tools.DoubleToString(env.CurrentThrottle, 2) // 6
							));
						}

					}
					else
					{
						env.CurrentThrottle = 0.4;
						if(Settings.Test) Console.WriteLine("### " + env.CurrentThrottle);
					}

					if (tick < 2)
					{
						env.CurrentThrottle = 0.6;
						if (Settings.Test) Console.WriteLine("### " + env.CurrentThrottle);
					}

					send(new Throttle(env.CurrentThrottle, Tools.GetGameTick(position, msg)));
					//send(new Throttle(0.7));
					break;

				#endregion

				case MessageType.turboAvailable:
					var turbo = Tools.FromJson<TurboAvailable>(line);
					Console.WriteLine("##### Turbo is now available at tick " + Tools.GetGameTickString(turbo, msg) + " #####");
					env.TurboAvailable = true;
					//send(new Ping());
					send(new Throttle(env.CurrentThrottle, Tools.GetGameTick(turbo, msg)));
					break;

				// 1
				case MessageType.join:
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case MessageType.yourCar:
					var yourCar = Tools.FromJson<YourCar>(line);
					if (yourCar != null && yourCar.data != null)
					{
						env.MyCar = new Car() { id = yourCar.data };
					}
					break;
				case MessageType.joinRace:
					Console.WriteLine("Joined: " + firstMessageToSend.ToString());
					send(new Ping());
					break;
				// 3
				case MessageType.gameInit:
					Console.WriteLine("Race init");

					GameInit gameInit = Tools.FromJson<GameInit>(line);
					if (gameInit != null && gameInit.data != null && gameInit.data.race != null && gameInit.data.race.track != null)
					{
						env.Track = gameInit.data.race.track;
						env.GameInitHoleData = gameInit;
					}

					//if (Settings.DefaultTrackName == "usa")
					//{
					//	send(new Throttle(1, Tools.GetGameTick(gameInit, msg)));
					//	break;
					//}

					//send(new Ping());
					send(new Throttle(env.CurrentThrottle, Tools.GetGameTick(gameInit, msg)));
					break;
				// 8
				case MessageType.gameEnd:
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				// 4
				case MessageType.gameStart:
					Console.WriteLine("Race starts");
					//send(new Ping());
					send(new Throttle(env.CurrentThrottle, Tools.GetGameTick(msg)));
					break;

				case MessageType.crash:
					Crash crash = Tools.FromJson<Crash>(line);
					if (crash != null)
					{
						Console.WriteLine(String.Format(
							"Crash! {0} at tick {1}. ##############################################################################################################################",
							crash.data != null && crash.data.name != null ? crash.data.name : String.Empty,
							Tools.GetGameTickString(crash)
						));
					}
					env.CurrentThrottleFactor = env.CurrentThrottleFactor - 0.1;
					send(new Ping());
					break;

				//yourCar = 2,
				//throttle = 6,
				//tournamentEnd = 9,
				//ping,
				//crash,
				//spawn,
				//lapFinished,
				//dnf,
				//finish,
				//switchLane,
				//unknown
				default:
					//send(new Ping());
					send(new Throttle(env.CurrentThrottle, Tools.GetGameTick(msg)));
					break;


			}
		}

		FinalizeBot();
	}

	private static void FinalizeBot()
	{
		
	}

	private void send(IMessage msg)
	{
		string json = msg.ToJson();
		LatestSentLine = json;

		if (Settings.Debug) Log.WriteToLog(json, LogEntryType.SentMessage);
		
		writer.WriteLine(json);
	}
}

//class MsgWrapper {
//	public string msgType;
//	public Object data;

//	public MsgWrapper(string msgType, Object data) {
//		this.msgType = msgType;
//		this.data = data;
//	}
//}

//abstract class SendMsg {
//	public string ToJson() {
//		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
//	}
//	protected virtual Object MsgData() {
//		return this;
//	}

//	protected abstract string MsgType();
//}

//class Join: SendMsg {
//	public string name;
//	public string key;
//	public string color;

//	public Join(string name, string key) {
//		this.name = name;
//		this.key = key;
//		this.color = "red";
//	}

//	protected override string MsgType() { 
//		return "join";
//	}
//}

//class Ping: SendMsg {
//	protected override string MsgType() {
//		return "ping";
//	}
//}

//class Throttle: SendMsg {
//	public double value;

//	public Throttle(double value) {
//		this.value = value;
//	}

//	protected override Object MsgData() {
//		return this.value;
//	}

//	protected override string MsgType() {
//		return "throttle";
//	}
//}